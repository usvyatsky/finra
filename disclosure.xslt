<?xml version="1.1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:csv="csv:csv">
    <xsl:output method="text" encoding="utf-8" />
    <xsl:strip-space elements="*" />

    <xsl:variable name="delimiter" select="'|'" />

    <csv:columns>
        <column>Report Number</column>
	<column>Company</column>
	<column>CRD</column>
	<column>Date</column>
        <column>Disclosure Type</column>
        <column>Disclosure Number</column>
	<column>Initiated By</column>
	<column>Date Initiated</column>
	<column>Principal Sanction(s)/Relief Sought</column>
	<column>Sanctions Ordered: Suspension (y/n)</column>
	<column>Sanctions Ordered: Fine (y/n)</column>
	<column>Sanctions Ordered: Fine $</column>
	<column>Sanctions Ordered: Cease and Desist (y/n)</column>
	<column>Other Sanctions Ordered: Suspension (y/n)</column>
	<column>Other Sanctions Ordered: Fine (y/n)</column>
	<column>Other Sanctions Ordered: Fine $</column>
	<column>Other Sanctions Ordered: Cease and Desist (y/n)</column>
    </csv:columns>

    <xsl:template match="report">
        <!-- Output the CSV header -->
        <xsl:for-each select="document('')/*/csv:columns/*">
                <xsl:value-of select="."/>
                <xsl:if test="position() != last()">
                    <xsl:value-of select="$delimiter"/>
                </xsl:if>
        </xsl:for-each>
        <xsl:text>&#xa;</xsl:text>
        <!-- Extract report number, date, CRD and company -->
        <xsl:variable name="reportNumber" select="./@number" />
        <xsl:variable name="reportCompany" select="./@company" />
        <xsl:variable name="reportCRD" select="./@crd" />
        <xsl:variable name="reportDate" select="./@date" />
        <!-- Loop through the disclosure types -->
        <xsl:for-each select="./disclosureType">
            <!-- Extract the disclosure type -->
            <xsl:variable name="disclosureType" select="./@name" />
            <!-- Loop through disclodures -->
            <xsl:for-each select="./disclosure">
                <!-- Extract disclosure number and note (if any) -->
                <xsl:variable name="disclosureNumber" select="./@number" />
                <xsl:variable name="initiatedBy">
                    <xsl:for-each select="./itemName">
                        <xsl:variable name="itemName" select="." />
                        <xsl:variable name="itemId" select="./@id" />
                        <xsl:variable name="itemValue" select="../itemValue[@id=$itemId]" />
                        <xsl:choose>
                            <xsl:when test="$itemName = 'Initiated By'">

                <!-- Now form the line -->
                <xsl:value-of select="$reportNumber" />
                <xsl:value-of select="$delimiter" />
                <xsl:value-of select="$reportCompany" />
                <xsl:value-of select="$delimiter" />
                <xsl:value-of select="$reportCRD" />
                <xsl:value-of select="$delimiter" />
                <xsl:value-of select="$reportDate" />
                <xsl:value-of select="$delimiter" />
                <xsl:value-of select="$disclosureType" />
                <xsl:value-of select="$delimiter" />
                <xsl:value-of select="$disclosureNumber" />
                <xsl:value-of select="$delimiter" />
		<!-- Loop through disclosure items -->
                
                <xsl:for-each select="./itemName">
                    <!-- Extract item id -->
                    <xsl:variable name="itemName" select="." />
                    <xsl:variable name="itemId" select="./@id" />
                    <xsl:variable name="itemValue" select="../itemValue[@id=$itemId]" />
                    <xsl:choose>
                        <xsl:when test="$itemName = 'Initiated By'">
                            <xsl:value-of select="$itemValue" />
                            <xsl:value-of select="$delimiter" />
                        </xsl:when>
                        <xsl:when test="$itemName = 'Date Initiated'">
                            <xsl:value-of select="$itemValue" />
                            <xsl:value-of select="$delimiter" />
                        </xsl:when>
                        <xsl:when test="$itemName = 'Principal Sanction(s)/Relief Sought'">
                            <xsl:value-of select="$itemValue" />
                            <xsl:value-of select="$delimiter" />
                        </xsl:when>
                        <xsl:when test="$itemName = 'Sanctions Ordered'">
                            <xsl:variable name="sanctionsOrderedSuspention">
                                <xsl:choose>
                                    <xsl:when test="matches($itemValue,'Suspension')">
                                        <xsl:value-of select="'y'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="'n'" />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="sanctionsOrderedCeaseAndDesist">
                                <xsl:choose>
                                    <xsl:when test="matches($itemValue,'Cease and Desist')">
                                        <xsl:value-of select="'y'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="'n'" />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="sanctionsOrderedFineYN">
                                <xsl:choose>
                                    <xsl:when test="matches($itemValue,'Fine')">
                                        <xsl:value-of select="'y'"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="'n'" />
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="sanctionsOrderedFineDollars">
                                <xsl:analyze-string select="$itemValue" regex=".*(\$[0-9,.]+).*">
                                    <xsl:matching-substring>
                                        <xsl:value-of name="fineDollars" select="regex-group(1)" />
                                    </xsl:matching-substring>
                                </xsl:analyze-string>
                            </xsl:variable>
                            <xsl:if test="matches($fineDollars,'\$[0-9,.]+')">
                                <xsl:variable name="fineYN" select="'y'" />
                            </xsl:if>
                            <xsl:if test="matches($itemValue,'Cease and Desist')">
                                <xsl:variable name="ceaseAndDesist" select="'y'" />
                            </xsl:if>
                            <xsl:value-of select="$suspention" />
                            <xsl:value-of select="$delimiter" />
                            <xsl:value-of select="$fineYN" />
                            <xsl:value-of select="$delimiter" />
                            <xsl:value-of select="$fineDollars" />
                            <xsl:value-of select="$delimiter" />
                            <xsl:value-of select="$ceaseAndDesist" />
                            <xsl:value-of select="$delimiter" />
                        </xsl:when>
                        <xsl:when test="$itemName = 'Other Sanctions Ordered'">
                            <xsl:variable name="suspention" select="'n'" />
                            <xsl:variable name="fineYN" select="'n'" />
                            <xsl:variable name="fineDollars" />
                            <xsl:variable name="ceaseAndDesist" select="'n'" />
                            <xsl:if test="matches($itemValue,'^Suspension.*')">
                                <xsl:variable name="suspention" select="'y'" />
                            </xsl:if>
                            <xsl:analyze-string select="$itemValue" regex=".*(\$[0-9,.]+).*">
                                <xsl:matching-substring>
                                    <xsl:variable name="fineDollars" select="regex-group(1)" />
                                </xsl:matching-substring>
                            </xsl:analyze-string>
                            <xsl:if test="matches($fineDollars,'\$[0-9,.]+')">
                                <xsl:variable name="fineYN" select="'y'" />
                            </xsl:if>
                            <xsl:if test="matches($itemValue,'Cease and Desist')">
                                <xsl:variable name="ceaseAndDesist" select="'y'" />
                            </xsl:if>
                            <xsl:value-of select="$suspention" />
                            <xsl:value-of select="$delimiter" />
                            <xsl:value-of select="$fineYN" />
                            <xsl:value-of select="$delimiter" />
                            <xsl:value-of select="$fineDollars" />
                            <xsl:value-of select="$delimiter" />
                            <xsl:value-of select="$ceaseAndDesist" />
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:value-of select="
                <xsl:text>&#xa;</xsl:text>
            </xsl:for-each>
        </xsl:for-each>
        <!-- Add a newline at the end of the record -->
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
</xsl:stylesheet>
