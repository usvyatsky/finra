<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:csv="csv:csv">
    <xsl:output method="text" encoding="utf-8" />
    <xsl:strip-space elements="*" />

    <xsl:variable name="delimiter" select="'|'" />

    <csv:columns>
        <column>Report Number</column>
	<column>Company</column>
	<column>CRD</column>
	<column>Date</column>
        <column>Disclosure Type</column>
        <column>Disclosure Number</column>
        <column>itemName</column>
        <column>itemValue</column>
        <column>note</column>
    </csv:columns>

    <xsl:template match="report">
        <!-- Output the CSV header -->
        <xsl:for-each select="document('')/*/csv:columns/*">
                <xsl:value-of select="."/>
                <xsl:if test="position() != last()">
                    <xsl:value-of select="$delimiter"/>
                </xsl:if>
        </xsl:for-each>
        <xsl:text>&#xa;</xsl:text>
        <!-- Extract report number, date, CRD and company -->
        <xsl:variable name="reportNumber" select="./@number" />
        <xsl:variable name="reportCompany" select="./@company" />
        <xsl:variable name="reportCRD" select="./@crd" />
        <xsl:variable name="reportDate" select="./@date" />
        <!-- Loop through the disclosure types -->
        <xsl:for-each select="./disclosureType">
            <!-- Extract the disclosure type -->
            <xsl:variable name="disclosureType" select="./@name" />
            <!-- Loop through disclodures -->
            <xsl:for-each select="./disclosure">
                <!-- Extract disclosure number and note (if any) -->
                <xsl:variable name="disclosureNumber" select="./@number" />
		<xsl:variable name="note" select="./note" />
		<!-- Loop through disclosure items -->
                <xsl:for-each select="./itemName">
                    <!-- Extract item id -->
                    <xsl:variable name="itemId" select="./@id" />
                    <!-- Now form the line -->
                    <xsl:value-of select="$reportNumber" />
                    <xsl:value-of select="$delimiter"/>
                    <xsl:value-of select="$reportCompany" />
                    <xsl:value-of select="$delimiter"/>
                    <xsl:value-of select="$reportCRD" />
                    <xsl:value-of select="$delimiter"/>
                    <xsl:value-of select="$reportDate" />
                    <xsl:value-of select="$delimiter"/>
                    <xsl:value-of select="$disclosureType" />
                    <xsl:value-of select="$delimiter"/>
                    <xsl:value-of select="$disclosureNumber" />
                    <xsl:value-of select="$delimiter"/>
                    <xsl:value-of select="." />
                    <xsl:value-of select="$delimiter"/>
                    <xsl:value-of select="../itemValue[@id=$itemId]" />
                    <xsl:value-of select="$delimiter"/>
                    <xsl:value-of select="$note"/>
                    <xsl:text>&#xa;</xsl:text>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:for-each>
        <!-- Add a newline at the end of the record -->
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
</xsl:stylesheet>
