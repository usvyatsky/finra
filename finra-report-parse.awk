#!/usr/bin/awk -f
# Copyright (c) 2015 - Ilya Usvyatsky

BEGIN {
	disclosure = 0;
	section = "none";
	report = 0;
	indent = "";
	report_indent = "  ";
	section_indent = report_indent "  ";
	disclosure_indent = section_indent "  ";
	company = "";
	crd = "";
	lid = 0;
	fid = 0;
}
function close_disclosure() {
	item_name = "";
	item_value = "";
	item_id = 1;
	item_name_ready = 0;
	for (i = 1; i <= lid; ++i) {
		c = index(item_name, ":");
		q = index(item_name, "?");
		if (!item_name_ready && (c || q) && (i == 1 || left[i - 1] != left[i])) {
			sub(":$", "", item_name);
			print indent "<itemName id=\"" item_id "\">" item_name "</itemName>";
			item_name_ready = 1;
		}
		if (item_name_ready && left[i] != "") {
			gsub("<", "\\&lt;", item_value);
			gsub(">", "\\&gt;", item_value);
			gsub("&", "\\&amp;", item_value);
			gsub("'", "\\&apos;", item_value);
			gsub("\"", "\\&quot;", item_value);
			print indent "<itemValue id=\"" item_id "\">" item_value "</itemValue>";
			item_name_ready = 0;
			item_name = "";
			item_value = "";
			item_id++;
		}
		if (item_name == "")
			item_name = left[i];
		else if (item_name != "" && (i == 1 || left[i - 1] != left[i]))
			item_name = item_name " " left[i];
		if (item_value == "")
			item_value = right[i];
		else if (right[i] != "" && (i == 1 || right[i - 1] != right[i]))
			item_value = item_value " " right[i];
	}
	if (item_name != "") {
		sub(":$", "", item_name);
		print indent "<itemName id=\"" item_id "\">" item_name "</itemName>";
		gsub("&", "&amp;", item_value);
		print indent "<itemValue id=\"" item_id "\">" item_value "</itemValue>";
		item_closed = 0;
		item_name = "";
		item_value = "";
	}
	if (fid) {
		note = full[1];
		for (i = 2; i <= fid; ++i)
			note = note " " full[i];
		gsub("&", "&amp;", note);
		print indent "<note>" note "</note>";
	}
	indent = section_indent;
	print indent "</disclosure>"
}
function close_section() {
	if (disclosure > 0)
		close_disclosure();
	indent = report_indent;
	print indent "</disclosureType>"
}
function close_report() {
	if (section != "none")
		close_section();
	indent = "";
	if (report)
		print indent "</report>"
	disclosure = 0;
	section = "none";
	report = 0
}
function new_report() {
	number = $2;
	comma = index(number, ",");
	if (comma)
		number = substr(number, 0, comma - 1);
	date = $7 " " $8 " " $9 " " $NF;
	period = index(date, ".");
	if (period)
		date = substr(date, 0, period - 1);
	print indent "<report number=\"" number "\" company=\"" company "\" crd=\"" crd "\" date=\"" date "\">"
	indent = report_indent;
	report = 1;
}
function new_section() {
	if (section != "none")
		close_section()
	section = $0;
	print indent "<disclosureType name=\"" section "\">";
	disclosure = 0;
	indent = section_indent
}
function new_disclosure() {
	if (disclosure > 0)
		close_disclosure();
	disclosure=$2;
	print indent "<disclosure number=\"" disclosure "\" of=\"" $4 "\" section=\"" section "\">";
	indent = disclosure_indent;
	lid = 0;
	fid = 0;
}
function disclosure_line() {
	if (match($0, "^([ \t]*|i)$"))
		return;
	if (length($0) <= 30) {
		left[++lid] = $0;
		right[lid] = "";
	} else {
		mid_str = substr($0, 31, 4);
		if (mid_str != "    ") {
			full[++fid] = $0;
			return;
		}
		left[++lid] = substr($0, 0, 30);
		right[lid] = substr($0, 40);
	}
	sub(" +$", "", left[lid]);
	sub("^ +", "", right[lid]);
}
(NR == 2) { company = $0; gsub("&", "&amp;",company); }
/^CRD#/ { crd = $2; gsub("&", "&amp;",crd);  }
/^Report #[0-9]+-[0-9]+/ && (report == 0) { new_report(); }
/^Regulatory - Final$/ { new_section() }
/^Regulatory - Pending$/ { new_section() }
/^Civil - Final$/ { new_section() }
/^Civil - On Appeal$/ { new_section() }
/^Arbitration Award - Award\/Judgment$/ { new_section() }
/End of Report/ { close_report() }
/^Disclosure [0-9]+ of [0-9]+$/ { new_disclosure(); next }
(disclosure > 0) { disclosure_line() }
