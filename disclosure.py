#!/usr/bin/env python

from xml.dom import minidom
import sys
import re

delimiter = "|"

def findIdByName(itemNames, name):
	for itemName in itemNames:
		if itemName.firstChild.data == name:
			return itemName.getAttribute("id")
	return ""

def findValueById(itemValues, id):
	for itemValue in itemValues:
		if itemValue.getAttribute("id") == id:
			if itemValue.firstChild:
				return itemValue.firstChild.data
			return ""
	return ""

def findValueByName(disclosure, name):
	itemNames = disclosure.getElementsByTagName("itemName")
	itemValues = disclosure.getElementsByTagName("itemValue")
	id = findIdByName(itemNames, name)
	return findValueById(itemValues, id)

def matchYN(pattern, text):
	if text:
		if re.match(".*" + pattern + ".*", text, re.IGNORECASE):
			return "y"
	return "n"

def searchDollars(text):
	if text:
		dollars = re.search("(?<=\$)([0-9][0-9,]+\.?[0-9]*)", text)
		if dollars:
			return dollars.group(0)
	return "0"

def parseSanctions(sanctionsText):
	fineYN = matchYN("Fine", sanctionsText)
	if fineYN == 'y':
		fineDollars = searchDollars(sanctionsText)
	else:
		fineDollars = "0"
	return [ matchYN("Suspension", sanctionsText),
		fineYN, fineDollars,
		matchYN("Cease and Desist", sanctionsText) ]

columns = [
	"Report Number",
	"Company",
	"CRD",
	"Date",
	"Disclosure Type",
	"Disclosure Number",
	"Initiated By",
	"Date Initiated",
	"Principal Sanction(s)/Relief Sought",
	"Sanctions Ordered: Suspension (y/n)",
	"Sanctions Ordered: Fine (y/n)",
	"Sanctions Ordered: Fine $",
	"Sanctions Ordered: Cease and Desist (y/n)",
	"Other Sanctions Ordered: Suspension (y/n)",
	"Other Sanctions Ordered: Fine (y/n)",
	"Other Sanctions Ordered: Fine $",
	"Other Sanctions Ordered: Cease and Desist (y/n)" ]

def parseDisclosure(disclosure):
	line = [ disclosure.getAttribute("number"),
		findValueByName(disclosure, "Initiated By"),
		findValueByName(disclosure, "Date Initiated"),
		findValueByName(disclosure, "Principal Sanction(s)/Relief Sought") ]
	sanctionsOrdered = findValueByName(disclosure, "Sanctions Ordered")
	line += parseSanctions(sanctionsOrdered)
	otherSanctionsOrdered = findValueByName(disclosure,
		"Other Sanctions Ordered")
	line += parseSanctions(otherSanctionsOrdered)
	return line

def parseReport(reportFile):
	doc = minidom.parse(reportFile)
	report = doc.getElementsByTagName("report")[0]
	reportNumber = report.getAttribute("number")
	company = report.getAttribute("company")
	crd = report.getAttribute("crd")
	date = report.getAttribute("date")
	reportPrefix = [ reportNumber, company, crd, date ]
	disclosureTypeObjs = report.getElementsByTagName("disclosureType")
	for disclosureTypeObj in disclosureTypeObjs:
		disclosureType = disclosureTypeObj.getAttribute("name")
		disclosureTypePrefix = reportPrefix + [ disclosureType ]
		disclosures = disclosureTypeObj.getElementsByTagName("disclosure")
		for disclosure in disclosures:
			disclosureLine = parseDisclosure(disclosure)
			print delimiter.join(disclosureTypePrefix + disclosureLine)

if len(sys.argv) != 2:
	print "Usage: ", sys.argv[0], " <xml_file>"
	exit(1)

print delimiter.join(columns)
parseReport(sys.argv[1])
