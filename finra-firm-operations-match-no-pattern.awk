#!/usr/bin/awk -f
# Copyright (c) 2015, 2016 - Ilya Usvyatsky

BEGIN {
	start = 0;
	getline entries[1] < list;
	nEntries = 1;
	while ((getline line < list) > 0) entries[++nEntries] = line;
	for (i = 1; i <= nEntries; ++i) {
		matched[entries[i]] = "n";
	}
}
match($0, starter) {
	start = 1;
}
match($0, stopper) {
	start = 0;
}
(start == 1) {
	for  (i = 1; i <= nEntries; ++i) {
		if (match($0, entries[i])) {
			matched[entries[i]] = "y";
			break;
		}
	}
}
END {
	for (i = 1; i <= nEntries; ++i)
		printf("%s%s", matched[entries[i]], sep);
}
