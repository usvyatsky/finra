#!/bin/bash
# Copyright (c) 2015,2016 - Ilya Usvyatsky

base=${1%.*}
pdftotext -enc ASCII7 -layout ${1}
cp ${base}.txt ${base}-orig.txt
awk '/FINRA. All rights reserved./{skip=1}/User Guidance$/{skip=0}{if (!skip) print}' ${base}.txt|grep -v 'User Guidance'|grep -v -e '©201[0-9]'|grep -v 'All rights reserved.   Report# '|grep -v 'Data current as of '|grep -v -e '^$' > ${base}-no-pgbrk.txt
awk 'BEGIN{prev=""}$0!=prev{prev=$0;print}' < ${base}-no-pgbrk.txt > ${base}-no-dups.txt
awk 'BEGIN{prev=""}{if (index($0,prev)!=1 && index(prev,$0)!=1) print prev;prev=$0}END{print prev}' < ${base}-no-dups.txt > ${base}-no-parts.txt
./finra-report-parse.awk ${base}-no-parts.txt > ${base}.xml
./disclosure.py ${base}.xml > ${base}.txt
