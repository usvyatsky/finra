#!/usr/bin/awk -f
# Copyright (c) 2015, 2016 - Ilya Usvyatsky

BEGIN {
	start = 0;
	getline entries[1] < list;
	nEntries = 1;
	while ((getline line < list) > 0) entries[++nEntries] = line;
	for (i = 1; i <= nEntries; ++i) {
		approved[entries[i]] = "n";
#		printf("%s%s", entries[i], sep);
	}
#	printf("\n");
}
match($0, starter) {
	start = 1;
}
match($0, pattern) && (start == 1) {
	entry = $1;
	for (i = 2; i < NF; ++i) {
		if (match($i, pattern))
			break;
		entry = entry " " $i;
	}
#	print(entry);
	approved[entry] = "y";
}
END {
	for (i = 1; i <= nEntries; ++i)
		printf("%s%s", approved[entries[i]], sep);
#	printf("\n");
}
