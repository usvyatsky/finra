#!/bin/bash
# Copyright (c) 2015, 2016 - Ilya Usvyatsky

# Report Summary:
#		Company
#		CRD#
#		SEC#
#                Firm classification (corp, llc .)
#                Firm formation date
#                Registered w SEC (y/n)
#                No SRO.s (1,2, .)
#                No States (1,2, ...)
#                No types of business (1,2, .28)
#                Affiliated (y/n)
#                Referral arrangements (y/n)
#                Disclosures (y/n)
#
#From .firm operations. pages:
#                Reg BD (y/n)
#                Reg BD & govt sec (y/n)
#                Reg govt sec only (y/n)
#                SRO (name given e.g.FINRA)
#       States:
#                Alabama (y/n)
#                Alaska (y/n)
#                Etc .
#       Types of business:
#                Mutual funds (y/n)
#                Tax shelters (y/n)
#                Life insurance (y/n)
#                Etc. There are 28 possible types of business
#        Clearing (y/n)
#        Introducing (y/n)

# Caption line
function caption_line() {
	rm -f aggregate.csv
	echo -n "Company;CRD #;SEC #;Classification;Date Formed;" > aggregate.csv
	echo -n "Registered With SEC;No SRO's;No States;" >> aggregate.csv
	echo -n "No types of business;Affiliated;Referral arrangements;" >> aggregate.csv
	echo -n "Disclosures;" >> aggregate.csv
	echo -n "Reg BD;Reg BD & govt sec;Reg govt sec only;Ceased govt sec;SRO's;" >> aggregate.csv

	while read i; do echo -n $i';'; done < finra-states.txt >> aggregate.csv
	while read i; do echo -n $i';'; done < finra-tob.txt >> aggregate.csv

	echo "Clearing;Introducing" >> aggregate.csv
}

function process_one_report() {
	base=${1%.*}
	echo ${base}
	rm -f ${base}*.txt ${base}*.csv
	pdftotext -enc ASCII7 -f 1 -l 1 -layout ${base}.pdf -> ${base}-TOC.txt
	rs_first=`awk '/Report Summary/{print $3 + 2}' ${base}-TOC.txt`
	rs_last=`awk '/Report Summary/{if (NF > 3) print $5 + 2; else print $3 + 2}' ${base}-TOC.txt`
	pdftotext -enc ASCII7 -f $rs_first -l $rs_last -raw ${base}.pdf -> ${base}-Report-Summary.txt
	fo_first=`awk '/Firm Operations/{print $3 + 2}' ${base}-TOC.txt`
	fo_last=`awk '/Firm Operations/{if (NF > 3) print $5 + 2; else print $3 + 2}' ${base}-TOC.txt`
	pdftotext -enc ASCII7 -f $fo_first -l $fo_last -raw ${base}.pdf -> ${base}-Firm-Operations-raw.txt
	pdftotext -enc ASCII7 -f $fo_first -l $fo_last -layout ${base}.pdf -> ${base}-Firm-Operations.txt
	./finra-report-summary.awk < ${base}-Report-Summary.txt >            \
		${base}-Report-Summary.csv
	./finra-firm-operations-match.awk                                    \
		-v starter='This firm is registered with the SEC as:'        \
		-v sep=';' -v list='finra-sec-reg.txt' -v pattern='Yes'      \
		< ${base}-Firm-Operations.txt >> ${base}-Firm-Operations.csv
	./finra-firm-operations-match-name.awk                               \
		-v starter='Self-Regulatory Organization'                    \
		-v sep='	' -v list='finra-sro.txt'                    \
		-v pattern='Approved' -v stopper='U.S. States'               \
		< ${base}-Firm-Operations.txt >> ${base}-Firm-Operations.csv
	echo -n ';' >> ${base}-Firm-Operations.csv
	./finra-firm-operations-match.awk -v starter='U.S. States'           \
		-v sep=';' -v list='finra-states.txt' -v pattern='Approved'  \
		< ${base}-Firm-Operations-raw.txt >> ${base}-Firm-Operations.csv
	./finra-firm-operations-match-no-pattern.awk                         \
		-v starter='Types of Business'                               \
		-v stopper='Other Types of Business'                         \
		-v sep=';' -v list='finra-tob.txt'                           \
		< ${base}-Firm-Operations.txt >> ${base}-Firm-Operations.csv
	if grep "`cat finra-clearing.txt`" ${base}-Firm-Operations.txt > /dev/null
	then
		echo -n "y;" >> ${base}-Firm-Operations.csv
	else
		echo -n "n;" >> ${base}-Firm-Operations.csv
	fi
	if grep "`cat finra-intro.txt`" ${base}-Firm-Operations.txt > /dev/null
	then
		echo -n "y" >> ${base}-Firm-Operations.csv
	else
		echo -n "n" >> ${base}-Firm-Operations.csv
	fi
	cat ${base}-Report-Summary.csv ${base}-Firm-Operations.csv >> aggregate.csv
	echo "" >> aggregate.csv
}

caption_line
for f in $*; do
	process_one_report $f
done

