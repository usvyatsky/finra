#!/usr/bin/awk -f
# Copyright (c) 2015, 2016 - Ilya Usvyatsky

BEGIN {
	company = "";
	crd = "";
	sec = "";
	classification = "";
	dateFormed = "";
	registeredWith = 0;
	registeredWithSEC = "n";
	sros = 0;
	states = 0;
	typesOfBusiness = 0;
	affiliated = "n";
	referralArrangements = "n";
	disclosures = "n";
	disclosuresNum = 0;
}
(NR == 1) { company = $0; }
(NR == 2) && !/^CRD#/ { company = sprintf("%s %s", company, $0); }
/^CRD#/ { crd = $2;  }
/^SEC#/ { sec = $2; }
/^This firm is classified as a/ { classification = substr($0, index($0, $7)); gsub("\\.$", "", classification); }
/^This firm was formed in/ { dateFormed = $NF; gsub("\\.$", "", dateFormed); }
/^This firm is registered with:/ { registeredWith = 1; }
(registeredWith == 1) && /^* the SEC/ { registeredWithSEC = "y"; }
(registeredWith == 1) && /^* [0-9]+ Self-Regulatory Organizations/ { sros = $2; }
(registeredWith == 1) && /^* [0-9]+ U.S. states and territories/ { states = $2; }
/^This firm conducts [0-9]+ type/ { typesOfBusiness = $4; }
/^This firm is affiliated with/ { affiliated = "y"; }
/^This firm has referral/ && /arrangements/ { referralArrangements = "y"; }
/^The following types of disclosures have been/ { disclosures = "y"; }
(disclosures == "y") && /^Regulatory Event/ { disclosuresNum += $3; }
(disclosures == "y") && /^Arbitration/ { disclosuresNum += $2; }
(disclosures == "y") && /^Civil Event/ { disclosuresNum += $3; }
END {
	printf("%s;%s;%s;%s;%s;%s;%d;%d;%d;%s;%s;%s;",
		company, crd, sec, classification, dateFormed,
		registeredWithSEC, sros, states, typesOfBusiness,
		affiliated, referralArrangements, disclosures);
}
