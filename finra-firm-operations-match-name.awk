#!/usr/bin/awk -f
# Copyright (c) 2015, 2016 - Ilya Usvyatsky

BEGIN {
	start = 0;
	getline entries[1] < list;
	nEntries = 1;
	while ((getline line < list) > 0) entries[++nEntries] = line;
	result = "";
#	printf("\n");
}
match($0, starter) {
	start = 1;
}
match($0, stopper) {
	start = 0;
}
match($0, pattern) && (start == 1) {
	entry = $1;
	for (i = 2; i < NF; ++i) {
		if (match($i, pattern))
			break;
		entry = entry " " $i;
	}
	for (i = 1; i <= nEntries; ++i) {
		if (entries[i] == entry) {
			if (result == "")
				result = entry;
			else
				result = result sep entry;
			break;
		}
	}
}
END {
	printf("%s", result);
}
